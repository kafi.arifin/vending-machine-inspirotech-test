import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import { Flex, Box } from 'rebass';

import Note from './note';

const allowedNotes = [2000, 5000, 10000 ,20000, 50000];

const ResetButton = styled(props => <Box {...props} width={1} mb={10}/>)`
  text-transform: uppercase;
  border: 2px solid #0986d3;
  color: #0986d3;
  height: 40px;
  border-radius: 6px;
  text-align: center;
  font-size: 20px;
  font-weight: bold;
  line-height: 36px;
  &:hover {
    background: #0986d3;
    color: #ffffff;
    cursor: pointer;
  }
`;

const MoneySlot = props => (
  <Flex justifyContent="space-between" flexWrap="wrap" width={220}>
    {allowedNotes.map(amount =>
      <Note
        key={amount}
        amount={amount}
        onClick={() => props.onCreditAdd(amount)}
      />
    )}
    <ResetButton onClick={() => props.onCreditReset()}>Kembalian</ResetButton>
  </Flex>
);

MoneySlot.propTypes = {
  onCreditAdd: PropTypes.func.isRequired,
  onCreditReset: PropTypes.func.isRequired
};

export default MoneySlot;
