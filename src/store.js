import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import reducers from './reducers';

const initialState = {
  credits: 0,
  selection: '',
  screen: {
    message: 'Selamat Datang!',
    type: 'info'
  },
  products: {
    'product1': {
      name: 'Biskuit',
      price: 6000
    },
    'product2': {
      name: 'Chips',
      price: 8000
    },
    'product3': {
      name: 'Oreo',
      price: 10000
    },
    'product4': {
      name: 'Tango',
      price: 12000
    },
    'product5': {
      name: 'Cokelat',
      price: 15000
    }
  },
  slots: {
    '1': {
      product: 'product1',
      amount: 1
    },
    '2': {
      product: 'product2',
      amount: 4
    },
    '3': {
      product: 'product3',
      amount: 4
    },
    '4': {
      product: 'product4',
      amount: 6
    },
    '5': {
      product: 'product5',
      amount: 5
    }
  }
};

export const history = createBrowserHistory({
  basename: process.env.PUBLIC_URL
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  reducers(history),
  initialState,
  composeEnhancers(applyMiddleware(thunk, routerMiddleware(history)))
);

export const { dispatch } = store;
